﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using TopDrivers.Helpers;

namespace TopDrivers
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type any key to start");
            Console.ReadLine();
            CalculateTopDrivers();
        }

        private static void CalculateTopDrivers()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            var drivers = DriverHelper.GetInstance().GetDriversFromCsv();
            var vehicles = VehicleHelper.GetInstance().GetVehiclesFromCsv();
            List<string> notfound;
            List<string> tripdata;
            var driverSpeeds = TripHelper.GetInstance().MapDriverSpeeds(out tripdata, out notfound);
            var driverAvgSpeeds = DriverHelper.GetInstance().GetDriverAvgSpeeds(drivers, driverSpeeds);
            var topDrivers = DriverHelper.GetInstance().GetTopDrivers(driverAvgSpeeds);

            int counter = 1;
            foreach(var driver in topDrivers)
            {
                Console.WriteLine(counter + ") " + drivers[driver.Key] + " - " + Math.Round(driver.Value,2).ToString() + " km/h");
                counter++;
            }

            Console.WriteLine();
            stopwatch.Stop();
            Console.WriteLine("Time to process data is {0}ms", stopwatch.ElapsedMilliseconds);

            // Add to write success and error text files:

            //string patherr = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Input Data\errors.txt");
            //File.WriteAllLines(patherr, notfound);
            //string pathtripdata = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Input Data\tripdata.txt");
            //File.WriteAllLines(pathtripdata, tripdata);

            Console.ReadLine();
        }
    }
}
