﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TopDrivers.Model
{
    public class Vehicle
    {
        public short Id { get; set; }
        public string Description { get; set; }
        public string RegistrationNo { get; set; }
    }
}
