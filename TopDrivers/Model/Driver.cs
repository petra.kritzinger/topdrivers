﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TopDrivers.Model
{
    public class Driver
    {
        public short Id { get; set; }
        public string Name { get; set; }

        public static Driver FromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Driver driver = new Driver();
            driver.Id = short.Parse(values[0]);
            driver.Name = values[1].Replace("\"", "");
            return driver;
        }
    }
}
