﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TopDrivers.Model
{
    public class VehicleDriverIdentification
    {
        public DateTime IdTime { get; set; }
        public short VehicleId { get; set; }
        public short DriverId { get; set; }
    }
}
