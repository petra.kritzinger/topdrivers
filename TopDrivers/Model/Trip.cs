﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TopDrivers.Model
{
    public class Trip
    {
        public short VehicleId { get; set; }
        public DateTime TripStart { get; set; }
        public DateTime TripEnd { get; set; }
        public float OdoStart { get; set; }
        public float OdoEnd { get; set; }
        public float Distance { get; set; }
        public long TripStartTicks { get; set; }
        public long TripEndTicks { get; set; }
        public short DriverId { get; set; }
        public Double Speed { get; set; }
    }
}
