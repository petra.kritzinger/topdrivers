﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TopDrivers.Helpers
{
    public class VehicleHelper
    {
        private static VehicleHelper _instance;

        private VehicleHelper()
        { }

        public static VehicleHelper GetInstance()
        {
            if (_instance == null)
            {
                _instance = new VehicleHelper();
            }
            return _instance;
        }

        /// <summary>
        /// Read vehicle list from csv into dictionary
        /// </summary>
        /// <returns></returns>
        public Dictionary<short, string> GetVehiclesFromCsv()
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Input Data\VehicleList.csv");

            Dictionary<short, string> drivers = File.ReadAllLines(path)
                                           .Select(v => VehicleRecFromCsv(v))
                                           .ToDictionary(x => x.Key, x => x.Value);

            return drivers;
        }

        public KeyValuePair<short, string> VehicleRecFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            return new KeyValuePair<short, string>(short.Parse(values[0]), values[1]);
        }
    }
}
