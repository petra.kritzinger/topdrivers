﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using TopDrivers.Model;

namespace TopDrivers.Helpers
{
    public class TripHelper
    {
        private static TripHelper _instance;

        private TripHelper()
        { }

        public static TripHelper GetInstance()
        {
            if (_instance == null)
            {
                _instance = new TripHelper();
            }
            return _instance;
        }

        /// <summary>
        /// Map all speeds relating to a specific driver to a dictionary list
        /// </summary>
        /// <param name="tripdata"></param>
        /// <param name="notfound"></param>
        /// <returns></returns>
        public Dictionary<short, List<double>> MapDriverSpeeds(out List<string> tripdata, out List<string> notfound)
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Input Data\Trips.dat");
            string pathout = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Input Data\Trips.txt");
            
            BinaryReader binReader = new BinaryReader(File.Open(path, FileMode.Open));
            
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            List<Trip> trips = new List<Trip>();

            var vehicleDrivers = ReadVehicleDriverIdentification();
            Dictionary<short, List<double>> driverSpeeds = new Dictionary<short, List<double>>();

            notfound = new List<string>();
            tripdata = new List<string>();

            while (binReader.BaseStream.Position != binReader.BaseStream.Length)
            {
                Trip trip = new Trip();
                trip.VehicleId = binReader.ReadInt16();
                long tripStart = binReader.ReadInt64();
                trip.TripStart = DateTime.FromBinary(tripStart);
                trip.TripStartTicks = trip.TripStart.Ticks;
                trip.OdoStart = binReader.ReadSingle();
                long tripEnd = binReader.ReadInt64();
                trip.TripEnd = DateTime.FromBinary(tripEnd);
                trip.TripEndTicks = trip.TripEnd.Ticks;
                trip.OdoEnd = binReader.ReadSingle();
                trip.Distance = binReader.ReadSingle();

                var vehicletrips = vehicleDrivers[trip.VehicleId].Keys.Where(x => x <= trip.TripEndTicks && x >= trip.TripStartTicks);
                if (vehicletrips.Count() > 0)
                {
                    trip.DriverId = vehicleDrivers[trip.VehicleId][vehicletrips.First()];

                    // Add to write success and error text files:
                    //tripdata.Add(trip.VehicleId + "," + trip.TripStart + "," + trip.TripEnd + "," + trip.DriverId);
                }
                else
                {
                    // Add to write success and error text files:
                    //notfound.Add(trip.VehicleId + "," + trip.TripStart + "," + trip.TripEnd);
                }

                var time = trip.TripEnd - trip.TripStart;
                trip.Speed = trip.Distance / time.Hours;

                trips.Add(trip);
                if (driverSpeeds.ContainsKey(trip.DriverId))
                {
                    driverSpeeds[trip.DriverId].Add(trip.Speed);
                }
                else
                {
                    driverSpeeds.Add(trip.DriverId, new List<double> { trip.Speed });
                }
            }
            binReader.Close();
            return driverSpeeds;
        }

        /// <summary>
        /// Read text file for vehicle driver identifications to a dictionary per vehicle
        /// </summary>
        /// <returns></returns>
        public Dictionary<short, Dictionary<long, short>> ReadVehicleDriverIdentification()
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Input Data\VehicleDriverIdentifications.txt");

            Dictionary<short, Dictionary<long, short>> vehicleDrivers = new Dictionary<short, Dictionary<long, short>>();
            

            var lines = File.ReadLines(path);
            foreach(var line in lines)
            {
                var i1 = line.IndexOf(" (");
                var i2 = line.IndexOf("::");
                var dt = DateTime.Parse(line.Substring(0, i1));
                var ticks = dt.Ticks;
                var vehicle = short.Parse(line.Substring(i1+2, i2 - i1 - 2));
                var driver = short.Parse(line.Substring(i2+2, line.Length - i2 - 3));
                if (vehicleDrivers.ContainsKey(vehicle))
                {
                    vehicleDrivers[vehicle].Add(ticks, driver);
                }
                else
                {
                    vehicleDrivers.Add(vehicle, new Dictionary<long, short>() { { ticks, driver } });
                }
            }

            return vehicleDrivers;
        }
    }
}
