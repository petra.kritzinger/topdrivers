﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using TopDrivers.Model;

namespace TopDrivers.Helpers
{
    public class DriverHelper
    {
        private static DriverHelper _instance;

        private DriverHelper()
        { }

        public static DriverHelper GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DriverHelper();
            }
            return _instance;
        }

        /// <summary>
        /// Read driver list from csv into dictionary
        /// </summary>
        /// <returns></returns>
        public Dictionary<short, string> GetDriversFromCsv()
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Input Data\DriverList.csv");

            Dictionary<short, string> drivers = File.ReadAllLines(path)
                                           .Select(v => DriverRecFromCsv(v))
                                           .ToDictionary(x => x.Key, x => x.Value);

            return drivers;
        }

        /// <summary>
        /// Calculate top drivers from driver average speed dictionary
        /// </summary>
        /// <param name="driverAvgSpeeds"></param>
        /// <returns></returns>
        internal IEnumerable<KeyValuePair<short, double>> GetTopDrivers(Dictionary<short, double> driverAvgSpeeds)
        {
            var driversSorted = driverAvgSpeeds.OrderByDescending(x => x.Value);
            return driversSorted.Take(10);
        }

        /// <summary>
        /// Calculate average speed per driver
        /// </summary>
        /// <param name="drivers"></param>
        /// <param name="driverSpeeds"></param>
        /// <returns></returns>
        internal Dictionary<short, double> GetDriverAvgSpeeds(Dictionary<short, string> drivers, Dictionary<short, List<double>> driverSpeeds)
        {
            var avgSpeeds = new Dictionary<short, double>();
            foreach (var driver in driverSpeeds)
            {
                avgSpeeds.Add(driver.Key, driver.Value.Average());
            }
            return avgSpeeds;
        }

        /// <summary>
        /// Read driver list from csv into list
        /// - I removed this function as the dictionary had faster lookup time
        /// </summary>
        /// <returns></returns>
        public List<Driver> GetDriversAsList()
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Input Data\DriverList.csv");

            List<Driver> drivers = File.ReadAllLines(path)
                                           .Select(v => Driver.FromCsv(v))
                                           .ToList();

            return drivers;
        }

        public KeyValuePair<short, string> DriverRecFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            return new KeyValuePair<short, string>(short.Parse(values[0]), values[1].Replace("\"", ""));
        }
    }
}
