The following assumptions were made:
- Two vehicles with the same ID were found (17779). The second one was changed to 17778. In the VehicleDriverIdentifications.txt file, the second set of captured values for 17779 was also changed to 17778.
- The binary file Trips.dat was specified as having a field End Odometer with type Odometer. Assuming this was meant to be a float, same as Start Odometer.

Errors found:
- 25073 records could be mapped to drivers (see tripdata.txt) but 22243 could not be mapped (see errors.txt). Unfortunately I didn't have time to investigate exactly why these trips could not be mapped. It could be due to a date time difference between the binary data and the vehicle driver identifications (e.g. different time zones).
- E.g. trip data for a trip with vehicle 22546 between 2019-10-31 12:04:54 AM and 2019-10-31 06:04:54 AM could be mapped to driver 1224. But a trip for the same vehicle between 2019-10-30 07:04:54 PM and 2019-10-30 11:04:54 PM could not be mapped

Output:
The output of my application was found to be the following. However, I cannot guarantee the accuracy of this output, as too many errors were present as described above.

Type any key to start

1) Devon Butler - 58,65 km/h
2) Flint Kelly - 57,97 km/h
3) Madison Jenkins - 57,56 km/h
4) Kordell Morris - 57,31 km/h
5) Liliana Kelly - 57,14 km/h
6) Adney Edwards - 57,07 km/h
7) Cabal Ward - 56,96 km/h
8) Shelbi Sanders - 56,79 km/h
9) Silver Morris - 56,72 km/h
10) Kady Reed - 56,72 km/h

Time to process data is 854ms